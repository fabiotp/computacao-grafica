# Projeto por Fabio Tepedino 
# Bibliotecas usadas: numpy.py, graphics.py e openpyxl.py

# Achei mais facil utilizar a biblioteca numpy.py para resolver as matrizes

from Ponto import*
from Reta import*
from Reta_Pontilhada import*
from Reta_Tracejada import*
from Circulo import*
from zBuffer import *
from texto import *
from aviao import *
from File import *
import time


# Cria listas com os textos dos angulos ao redor dos circulos.
textoAngulos45 = textoCircular(425,0,0,45)      
textoAngulos15 = textoCircular(460,0,0,15)

# As proximas funcoes printam a base do radar.
texto(win, 90, 980, "Feito por: Fabio Tepedino", 10, SuperVerde, "italic")
Circulo(0,0,100)
Circulo(0,0,200)
Circulo(0,0,300)
Circulo(0,0,400)
Reta_Tracejada(-400,0,400,0)
Reta_Tracejada(0,400,0,-400)
Reta_Tracejada(-283,283,283,-283)
Reta_Tracejada(-283,-283,283,283)
printVariousAngles(textoAngulos45,45, win)
printVariousAngles(textoAngulos15,15,win,15,"green","italic")


# Com tudo pronto, copia-se tudo do buffer1 para o fundo de Tela
FundoTela()
# Eh necessario uma primeira funcao para forcar os pontos do buffer a printar na tela.
# Ate agora, so estavamos copiando cores no buffer1.
# Agora precisamos chamar a funcar de plotagem da biblioteca graphics.
InitializeBuffer()


# Nao quis criar os icones dos avioes na mao.
# Entre com o angulo que deseja incrementar. 
# Ex: de 15 em 15 graus. 
createAircraftIcons(15)


# O programa comeca com os avioes em tempo 0
tempo = 0


# Inicializa a plotagem dos avioes em tempo 0
PrintPlanesAtTempo(tempo)


while tempo < 160:
    # Plota o texto no topo da tela
    t = textoRetorno(win, 50, 10, "Tempo: " + str(tempo) + "/150", 10, SuperVerde, "normal")

    # Compara todos os elementos do buffer ativo com o anterior.
    # Caso esteja diferente, eh plotado o novo ponto.  
    verifyCurrentBuffer()

    # O sistema espera 3 segundos
    time.sleep(3.0)
    
    # Troca o buffer ativo.
    BufferSwitch()
    tempo += 10

    # Eh necessario apagar os textos dos avioes, para que possam acompanha-los.
    apagarTextosAvioes()

    # Plota os avioes no buffer ativo.
    PrintPlanesAtTempo(tempo)

    # Apagar o texto "t"
    t.undraw()
 

