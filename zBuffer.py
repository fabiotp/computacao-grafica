# Projeto por Fabio Tepedino 

from graphics import *
import numpy as np

# Utilizei duas matrizes como buffer.
# Buffer1 e Buffer2.
# Fiz isto para poder utiliza-los ao mesmo tempo sem causar problemas
# Enquanto o buffer1 eh printado, os avioes do proximo frame sao gravados no buffer2
# No proximo frame, invertesse o buffer a ser lido, e o escrito.
# Esta troca ocorre durante todo o processo do programa

# Setando o buffer ativo
activeBuffer = 1

# Montando primeira tela com a biblioteca Graphics
sizeXWindow = 1000
sizeYWindow = 1000
backgroundColor = color_rgb(16,16,37)
win = GraphWin("Tela Radar", sizeXWindow, sizeYWindow)
win.setBackground(backgroundColor  )


# Inicializando os 2 buffer e uma matriz de fundo de tela.
buffer1 = np.ndarray(shape=(sizeXWindow,sizeYWindow), dtype='<U10')
buffer2 = np.ndarray(shape=(sizeXWindow,sizeYWindow), dtype='<U10')

# O fundoTela eh a base do radar, somente com texto, retas e circulos.
fundoTela = np.ndarray(shape=(sizeXWindow,sizeYWindow), dtype='<U10')

# Assinalando uma cor base a todos os elementos dos buffers e matriz fundo de tela
for i in range(sizeXWindow):
    for j in range(sizeYWindow):
        buffer1[i,j] = backgroundColor
        buffer2[i,j] = backgroundColor
        fundoTela[i,j] = backgroundColor
# Ate aqui o processo eh feito antes do programa comecar a rodar.

# Printa todos os pontos do buffer1.
def InitializeBuffer():
    global activeBuffer
    if activeBuffer == 1:  
        for i in range(sizeXWindow):
            for j in range(sizeYWindow):
                if buffer1[i,j] != backgroundColor:
                    win.plotPixel(i,j,buffer1[i,j])
    else:
        for i in range(sizeXWindow):
            for j in range(sizeYWindow):
                if buffer2[i,j] != backgroundColor:
                    win.plotPixel(i,j,buffer2[i,j])

# Apaga o buffer desejado, igualando todos seus elementos ao fundo da tela
# No final temos no buffer, a base do radar (circulos, retas e textos.)
def apagarTela(bufferNumber):
    for i in range(sizeXWindow):
            for j in range(sizeYWindow):
                if activeBuffer == 1:
                    buffer1[i,j] = fundoTela[i,j]
                else:
                    buffer2[i,j] = fundoTela[i,j]

# Compara todos os elementos do buffer ativo com o anterior.
# Caso esteja diferente, eh plotado o novo ponto.  
def verifyCurrentBuffer():
    global activeBuffer
    if activeBuffer == 1:  
        for i in range(sizeXWindow):
            for j in range(sizeYWindow):
                if buffer1[i,j] != buffer2[i,j]:
                    win.plotPixel(i,j,buffer1[i,j])
    else:
        for i in range(sizeXWindow):
            for j in range(sizeYWindow):
                if buffer2[i,j] != buffer1[i,j]:
                    win.plotPixel(i,j,buffer2[i,j])

# Copia o valor "cor" para o index [x,y] do buffer ativo neste instante        
def printOnActiveBuffer(cor, x, y):
    x = int(x)
    y = int(y)
    try:
        if activeBuffer == 1:
            buffer1[x,y] = cor
        else:
            buffer2[x,y] = cor
    except IndexError:
        print("Valor de x ", x, " Valor de y ", y, " Valor da cor ", cor)
        raise

# Cria o fundo de tela.
# Apos inicilizado o buffer1 com todos os elementos base, (circulos, retas, textos)
# Copiam-se todos estes elementos para o fundoTela e buffer2.
def FundoTela():
    for i in range(sizeXWindow):
            for j in range(sizeYWindow):
                fundoTela[i,j] = buffer1[i,j]
                buffer2[i,j] = buffer1[i,j]
                
# Troca qual o buffer ativo e apaga a tela
def BufferSwitch():
    global activeBuffer
    if activeBuffer == 1:
        activeBuffer = 2
        apagarTela(1)
    else:
        activeBuffer = 1
        apagarTela(2)
    return