# Projeto por Fabio Tepedino 

from graphics import*
from zBuffer import *

# Plota um ponto de 5 pixels no buffer ativo neste instante.
def pontos5Px(eixoX, eixoY):
    printOnActiveBuffer(corPonto, eixoX, eixoY)
    for i in range(2):
        auxY = eixoY + pow(-1, i)
        printOnActiveBuffer(corPonto, eixoX, auxY)
        auxX = eixoX + pow(-1, i)
        printOnActiveBuffer(corPonto, auxX, eixoY)
    return

# Plota um ponto de 1 pixels no buffer ativo neste instante.
def pontos1Px(eixoX, eixoY):
    printOnActiveBuffer(corPonto, eixoX, eixoY)
    return

# Plota um ponto de 4 pixels no buffer ativo neste instante.
def pontos4Px(eixoX, eixoY):
    for i in range(2):
        for j in range(2):
            printOnActiveBuffer(corPonto, eixoX+i, eixoY+j)
    return

# Plota um ponto de 12 pixels no buffer ativo neste instante.
def pontos12Px(eixoX, eixoY):
    for i in range (2):
        printOnActiveBuffer(corPonto, eixoX+i, eixoY-1)
        printOnActiveBuffer(corPonto, eixoX+i, eixoY+2)
        for j in range (4):
            printOnActiveBuffer(corPonto, eixoX+j-1, eixoY+i)
    return

# Corrigi o valor para adaptar a biblioteca a um novo sistema de coordenadas, em que 0,0 fica no centro da tela
def correctionX(eixoX):
    return eixoX + sizeXWindow/2

# Corrigi o valor para adaptar a biblioteca a um novo sistema de coordenadas, em que 0,0 fica no centro da tela
def correctionY(eixoY):
    return sizeYWindow/2 - eixoY

# Funcao que administra qual das outras deve ser chamada, dependendo da quantidade de pixel.
def Ponto(x, y, espessura = 1, cor ="white"):
    global corPonto
    corPonto = cor
    if espessura == 1:
        pontos1Px(correctionX(x),correctionY(y))
    elif espessura == 4:
        pontos4Px(correctionX(x), correctionY(y))
    elif espessura == 5:
        pontos5Px(correctionX(x), correctionY(y))
    elif espessura == 12:
        pontos12Px(correctionX(x), correctionY(y))
    return





