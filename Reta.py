# Projeto por Fabio Tepedino 

from Ponto import *

# Plota uma reta no buffer ativo, via funcao Ponto()
def Reta(x1,y1,x2,y2,espessura = 1, cor="white"):
    x = x1
    y = y1
    p = 0
    dX = x2-x1
    dY = y2-y1
    xInc = 1
    yInc = 1

    if dX<0 :
        xInc = -1; dX = -dX
    if dY<0 :
        yInc = -1; dY = -dY
    if dY <= dX:
        p = dX/2
        while int(x) != int(x2):
            Ponto(x,y,espessura, cor)
            p=p-dY
            if p<0 :
                y = y+yInc
                p=p+dX
            x=x+xInc
    else:
        p = dY/2
        while int(y) != int(y2):
            Ponto(x,y,espessura, cor)
            p = p-dX
            if p<0:
                x = x+xInc
                p = p+dY
            y = y+yInc
        Ponto(x,y,espessura, cor)    
    return
