# Projeto por Fabio Tepedino 

from graphics import *
from zBuffer  import *
import math

# Lista dos textos dos avioes, para podermos deleta-las quando necessario.
listaTextos = []

# Plota um texto utilizando a funcao da biblioteca Graphics.py
def texto(window, x, y, palavra, tamanho = 10, cor = "white", estilo="bold"):
    t = Text(Point(x,y), palavra)
    t.setOutline(cor)
    t.setSize(tamanho)
    t.setStyle(estilo)
    t.draw(window)
    return

# Retorna o texto criado. 
# Usada quando necessario manter o valor do objeto texto, para deletado ou edita-lo
def textoRetorno(window, x, y, palavra, tamanho = 10, cor = "white", estilo="bold"):
    t = Text(Point(x,y), palavra)
    t.setOutline(cor)
    t.setSize(tamanho)
    t.setStyle(estilo)
    t.draw(window)
    return t

# Corrigi o valor para adaptar a biblioteca a um novo sistema de coordenadas, em que 0,0 fica no centro da tela
def correctionX(eixoX):
    return eixoX + sizeXWindow/2
# Corrigi o valor para adaptar a biblioteca a um novo sistema de coordenadas, em que 0,0 fica no centro da tela
def correctionY(eixoY):
    return sizeYWindow/2 - eixoY

# Funcao para plotar os textosdos angulos do radar.
# Necessario entrar com os valores do raio, centro, e incremento dos angulos.
# Ex: Em main.py temos: "textoAngulos15 = textoCircular(460,0,0,15)"
# Este eh o texto mais externos com os valores dos angulos, de 15 em 15 graus.
def textoCircular(raio, centroX, centroY, anguloIcremento):
    angulo = 0
    anguloIncremento = math.radians(anguloIcremento)
    centroX = correctionX(centroX)
    centroY = correctionY(centroY)
    degtoRad360 = math.radians(360)
    listaCoordenadas = []


    while round(angulo,6) < round(degtoRad360,6):
        seno = math.sin(angulo)
        y = raio * seno
        cosseno = math.cos(angulo)
        x = raio * cosseno
        listaCoordenadas.append(correctionX(int(x)))
        listaCoordenadas.append(correctionY(int(y)))
        angulo += anguloIncremento
    return listaCoordenadas

# Plota os agulos circulares descritores anteriormente, utilizando a biblioteca graphics.py
def printVariousAngles(listaCoordenadas, incremento, window, tamanho = 10, cor = "white", estilo="bold"):
    qtd = int(len(listaCoordenadas))
    angulo = 90
    i = 0
    while i < (qtd):
        texto(window,listaCoordenadas[i], listaCoordenadas[i+1], str(angulo), tamanho, cor, estilo)
        if(angulo == 0):
            angulo = 360
        i += 2
        angulo -= incremento
        